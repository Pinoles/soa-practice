const express = require('express')
const serverless = require('serverless-http')
const cors = require('cors')
//const bodyparser = require('body-parser')
const clienteController = require('./routes/cliente');
const restauranteController = require('./routes/restaurante');
const comidaController = require('./routes/comida');
const inventarioController = require('./routes/inventario');
const ordenController = require('./routes/orden');

//var jsonParser = bodyparser.json();
const app = express();
const morgan = require('morgan');
app.use(morgan('tiny'));
app.use(cors());
app.use(express.urlencoded({extended: true})); 
app.use(express.json());

const path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

app.get("/", function (req, res) {
    res.send({ "stage": "dev" })
})

app.use('/api/cliente',clienteController);
app.use('/api/restaurante',restauranteController);
app.use('/api/comida', comidaController);
app.use('/api/inventario', inventarioController);
app.use('/api/orden', ordenController);

const history = require('connect-history-api-fallback');
app.use(history());
app.use(express.static(path.join(__dirname, 'public')));

var server = app.listen(4000, function () {
    console.log("Corriendo en localhost:4000")
})

module.exports.handler = serverless(app);
