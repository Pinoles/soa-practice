const express = require('express');
var router = express.Router();

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')

var dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

router.post("/createOrden", function (req, res) {
    let id = uuidv4();
    let date = new Date().toLocaleString('es-MX', { timeZone: 'America/Mexico_City' });

    (async () => {
        try {
            
            let data = {
                "pk": "Orden",
                "sk": id,
                "nombreOrden": req.body.nombreOrden,
                "precioTotal": req.body.precioTotal,
                "comidas_sk": req.body.comidas_sk,
                "usuario_sk":req.body.usuario_sk,
                "restaurant_sk": req.body.restaurant_sk,
                "createdAt": date
            };

            var params = {
                Item: data,
                ReturnConsumedCapacity: "TOTAL",
                TableName: "Restaurant_System"
            };

            dynamodb.put(params, function (err, response) {
                if (err) res.status(500).send(err);
                else {
                    res.status(200).send(data);
                }
            })
        } catch (error) {
            return res.status(500).send(error);
        }
    })()
})

router.get("/getAll", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        KeyConditionExpression: "pk = :pk",
        ExpressionAttributeValues: {
            ":pk": "Orden"
        },
        ExpressionAttributeNames: {
            "#pk": "pk"
        },
        FilterExpression: "#pk = :pk"
    }

    dynamodb.scan(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response.Items)
        }
    })
})

router.get("/getOrden/:sk/:nombreOrden", function (req, res) {
    (async () => {
        try {
            var params = {
                TableName: "Restaurant_System",
                KeyConditionExpression: "pk = :pk AND sk = :sk",
                ExpressionAttributeValues: {
                    ":pk": "Orden",
                    ":sk": req.params.sk,
                    ":nombreOrden": req.params.nombreOrden
                },
                ExpressionAttributeNames: {
                    "#nombreOrden": "nombreOrden"
                },
                FilterExpression: "#nombreOrden = :nombreOrden"
            };

            dynamodb.query(params, function (err, response) {
                if (err) res.status(500).send(err)
                else {
                    res.status(200).send(response.Items[0])
                }
            })
        } catch (err) {
            res.status(500).send(err)
        }
    })()
})

router.put("/updateOrden/:sk", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Orden",
            "sk": req.params.sk
        },
        UpdateExpression: "set precioTotal = :a, contrasena = :c, comidas_sk = :e, nombreOrden = :n",
        ExpressionAttributeValues: {
            ":n": req.body.nombreOrden,
            ":a": req.body.precioTotal,
            ":e": req.body.comidas_sk,
            ":c":req.body.contrasena,
        },
        ReturnValues: "UPDATED_NEW"
    };

    dynamodb.update(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response)
        }
    })
})

router.delete("/deleteOrden/:sk", function(req, res){
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Orden",
            "sk": req.params.sk
        }
    };

    dynamodb.delete(params, function(err, response){
        if (err) res.status(500).send(err)
        else {
            res.status(200).send("Deleted")
        }
    })
})

module.exports = router;