const express = require('express');
var router = express.Router();

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')

var dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

router.post("/createInventario", function (req, res) {
    let id = uuidv4();
    let date = new Date().toLocaleString('es-MX', { timeZone: 'America/Mexico_City' });

    (async () => {
        try {
            
            let data = {
                "pk": "Inventario",
                "sk": id,
                "nombreInventario": req.body.nombreInventario,
                "precio": req.body.precio,
                "cantidad": req.body.cantidad,
                "resataurante_sk": req.body.resataurante_sk,
                "createdAt": date
            };

            var params = {
                Item: data,
                ReturnConsumedCapacity: "TOTAL",
                TableName: "Restaurant_System"
            };

            dynamodb.put(params, function (err, response) {
                if (err) res.status(500).send(err);
                else {
                    res.status(200).send(data);
                }
            })
        } catch (error) {
            return res.status(500).send(error);
        }
    })()
})

router.get("/getAll", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        KeyConditionExpression: "pk = :pk",
        ExpressionAttributeValues: {
            ":pk": "Inventario"
        },
        ExpressionAttributeNames: {
            "#pk": "pk"
        },
        FilterExpression: "#pk = :pk"
    }

    dynamodb.scan(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response.Items)
        }
    })
})

router.get("/getInventario/:sk/:nombreInventario", function (req, res) {
    (async () => {
        try {
            var params = {
                TableName: "Restaurant_System",
                KeyConditionExpression: "pk = :pk AND sk = :sk",
                ExpressionAttributeValues: {
                    ":pk": "Inventario",
                    ":sk": req.params.sk,
                    ":nombreInventario": req.params.nombreInventario
                },
                ExpressionAttributeNames: {
                    "#nombreInventario": "nombreInventario"
                },
                FilterExpression: "#nombreInventario = :nombreInventario"
            };

            dynamodb.query(params, function (err, response) {
                if (err) res.status(500).send(err)
                else {
                    res.status(200).send(response.Items[0])
                }
            })
        } catch (err) {
            res.status(500).send(err)
        }
    })()
})

router.put("/updateInventario/:sk", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Inventario",
            "sk": req.params.sk
        },
        UpdateExpression: "set precio = :a, cantidad = :e, nombreInventario = :n",
        ExpressionAttributeValues: {
            ":n": req.body.nombreInventario,
            ":a": req.body.precio,
            ":e": req.body.cantidad
        },
        ReturnValues: "UPDATED_NEW"
    };

    dynamodb.update(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response)
        }
    })
})

router.delete("/deleteInventario/:sk", function(req, res){
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Inventario",
            "sk": req.params.sk
        }
    };

    dynamodb.delete(params, function(err, response){
        if (err) res.status(500).send(err)
        else {
            res.status(200).send("Deleted")
        }
    })
})

module.exports = router;