const express = require('express');
var router = express.Router();

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')

var dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

router.post("/createCliente", function (req, res) {
    let id = uuidv4();
    let date = new Date().toLocaleString('es-MX', { timeZone: 'America/Mexico_City' });

    (async () => {
        try {
            
            let data = {
                "pk": "Cliente",
                "sk": id,
                "nombreCliente": req.body.nombreCliente,
                "apellidos": req.body.apellidos,
                "email": req.body.email,
                "contrasena":req.body.contrasena,
                "createdAt": date
            };

            var params = {
                Item: data,
                ReturnConsumedCapacity: "TOTAL",
                TableName: "Restaurant_System"
            };

            dynamodb.put(params, function (err, response) {
                if (err) res.status(500).send(err);
                else {
                    res.status(200).send(data);
                }
            })
        } catch (error) {
            return res.status(500).send(error);
        }
    })()
})

router.get("/getAll", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        KeyConditionExpression: "pk = :pk",
        ExpressionAttributeValues: {
            ":pk": "Cliente"
        },
        ExpressionAttributeNames: {
            "#pk": "pk"
        },
        FilterExpression: "#pk = :pk"
    }

    dynamodb.scan(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response.Items)
        }
    })
})

router.get("/getCliente/:sk/:nombreCliente", function (req, res) {
    (async () => {
        try {
            var params = {
                TableName: "Restaurant_System",
                KeyConditionExpression: "pk = :pk AND sk = :sk",
                ExpressionAttributeValues: {
                    ":pk": "Cliente",
                    ":sk": req.params.sk,
                    ":nombreCliente": req.params.nombreCliente
                },
                ExpressionAttributeNames: {
                    "#nombreCliente": "nombreCliente"
                },
                FilterExpression: "#nombreCliente = :nombreCliente"
            };

            dynamodb.query(params, function (err, response) {
                if (err) res.status(500).send(err)
                else {
                    res.status(200).send(response.Items[0])
                }
            })
        } catch (err) {
            res.status(500).send(err)
        }
    })()
})

router.put("/updateCliente/:sk", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Cliente",
            "sk": req.params.sk
        },
        UpdateExpression: "set apellidos = :a, contrasena = :c, email = :e, nombreCliente = :n",
        ExpressionAttributeValues: {
            ":n": req.body.nombreCliente,
            ":a": req.body.apellidos,
            ":e": req.body.email,
            ":c":req.body.contrasena,
        },
        ReturnValues: "UPDATED_NEW"
    };

    dynamodb.update(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response)
        }
    })
})

router.delete("/deleteCliente/:sk", function(req, res){
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Cliente",
            "sk": req.params.sk
        }
    };

    dynamodb.delete(params, function(err, response){
        if (err) res.status(500).send(err)
        else {
            res.status(200).send("Deleted")
        }
    })
})

module.exports = router;