const express = require('express');
var router = express.Router();

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')

var dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

router.post("/createRestaurante", function (req, res) {
    let id = uuidv4();
    let date = new Date().toLocaleString('es-MX', { timeZone: 'America/Mexico_City' });

    (async () => {
        try {
            
            let data = {
                "pk": "Restaurante",
                "sk": id,
                "nombreRestaurante": req.body.nombreRestaurante,
                "telefono": req.body.telefono,
                "contactoRestaurante": req.body.contactoRestaurante,
                "createdAt": date
            };

            var params = {
                Item: data,
                ReturnConsumedCapacity: "TOTAL",
                TableName: "Restaurant_System"
            };

            dynamodb.put(params, function (err, response) {
                if (err) res.status(500).send(err);
                else {
                    res.status(200).send(data);
                }
            })
        } catch (error) {
            return res.status(500).send(error);
        }
    })()
})

router.get("/getAll", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        KeyConditionExpression: "pk = :pk",
        ExpressionAttributeValues: {
            ":pk": "Restaurante"
        },
        ExpressionAttributeNames: {
            "#pk": "pk"
        },
        FilterExpression: "#pk = :pk"
    }

    dynamodb.scan(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response.Items)
        }
    })
})

router.get("/getRestaurante/:sk/:nombreRestaurante", function (req, res) {
    (async () => {
        try {
            var params = {
                TableName: "Restaurant_System",
                KeyConditionExpression: "pk = :pk AND sk = :sk",
                ExpressionAttributeValues: {
                    ":pk": "Restaurante",
                    ":sk": req.params.sk,
                    ":nombreRestaurante": req.params.nombreRestaurante
                },
                ExpressionAttributeNames: {
                    "#nombreRestaurante": "nombreRestaurante"
                },
                FilterExpression: "#nombreRestaurante = :nombreRestaurante"
            };

            dynamodb.query(params, function (err, response) {
                if (err) res.status(500).send(err)
                else {
                    res.status(200).send(response.Items[0])
                }
            })
        } catch (err) {
            res.status(500).send(err)
        }
    })()
})

router.put("/updateRestaurante/:sk", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Restaurante",
            "sk": req.params.sk
        },
        UpdateExpression: "set telefono = :t, contactoRestaurante= :c, nombreRestaurante = :n",
        ExpressionAttributeValues: {
            ":n": req.body.nombreRestaurante,
            ":t": req.body.telefono,
            ":c": req.body.contactoRestaurante,
        },
        ReturnValues: "UPDATED_NEW"
    };

    dynamodb.update(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response)
        }
    })
})

router.delete("/deleteRestaurante/:sk", function(req, res){
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Restaurante",
            "sk": req.params.sk
        }
    };

    dynamodb.delete(params, function(err, response){
        if (err) res.status(500).send(err)
        else {
            res.status(200).send("Deleted")
        }
    })
})

module.exports = router;