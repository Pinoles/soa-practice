const express = require('express');
var router = express.Router();

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')

var dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

router.post("/createComida", function (req, res) {
    let id = uuidv4();
    let date = new Date().toLocaleString('es-MX', { timeZone: 'America/Mexico_City' });

    (async () => {
        try {
            
            let data = {
                "pk": "Comida",
                "sk": id,
                "nombreComida": req.body.nombreComida,
                "contenido": req.body.contenido,
                "peso": req.body.peso,
                "precio":req.body.precio,
                "restaurant_sk": req.body.restaurant_sk,
                "createdAt": date
            };

            var params = {
                Item: data,
                ReturnConsumedCapacity: "TOTAL",
                TableName: "Restaurant_System"
            };

            dynamodb.put(params, function (err, response) {
                if (err) res.status(500).send(err);
                else {
                    res.status(200).send(data);
                }
            })
        } catch (error) {
            return res.status(500).send(error);
        }
    })()
})

router.get("/getAll", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        KeyConditionExpression: "pk = :pk",
        ExpressionAttributeValues: {
            ":pk": "Comida"
        },
        ExpressionAttributeNames: {
            "#pk": "pk"
        },
        FilterExpression: "#pk = :pk"
    }

    dynamodb.scan(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response.Items)
        }
    })
})

router.get("/getComida/:sk/:nombreComida", function (req, res) {
    (async () => {
        try {
            var params = {
                TableName: "Restaurant_System",
                KeyConditionExpression: "pk = :pk AND sk = :sk",
                ExpressionAttributeValues: {
                    ":pk": "Comida",
                    ":sk": req.params.sk,
                    ":nombreComida": req.params.nombreComida
                },
                ExpressionAttributeNames: {
                    "#nombreComida": "nombreComida"
                },
                FilterExpression: "#nombreComida = :nombreComida"
            };

            dynamodb.query(params, function (err, response) {
                if (err) res.status(500).send(err)
                else {
                    res.status(200).send(response.Items[0])
                }
            })
        } catch (err) {
            res.status(500).send(err)
        }
    })()
})

router.put("/updateComida/:sk", function (req, res) {
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Comida",
            "sk": req.params.sk
        },
        UpdateExpression: "set contenido = :c, precio = :pr, peso = :pe, nombreComida = :n",
        ExpressionAttributeValues: {
            ":n": req.body.nombreComida,
            ":c": req.body.contenido,
            ":pe": req.body.peso,
            ":pr":req.body.precio,
        },
        ReturnValues: "UPDATED_NEW"
    };

    dynamodb.update(params, function (err, response) {
        if (err) res.status(500).send(err)
        else {
            res.status(200).send(response)
        }
    })
})

router.delete("/deleteComida/:sk", function(req, res){
    var params = {
        TableName: "Restaurant_System",
        Key: {
            "pk": "Comida",
            "sk": req.params.sk
        }
    };

    dynamodb.delete(params, function(err, response){
        if (err) res.status(500).send(err)
        else {
            res.status(200).send("Deleted")
        }
    })
})

module.exports = router;